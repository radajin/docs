# git lab keygen 설정

## 1. 생성

- 키 생성시 입력하는 패스워드는 잊어버리면 안된다.

```
$ ssh-keygen -t rsa -C "pdj1224@gmail.com" -b 4096
```

## 2. 확인

```
$ cat ~/.ssh/id_rsa.pub
```

## 3. 추가

- gitlab 페이지의 user settings - SSH Keys 메뉴에서 위에서 나온 키 문자열 추가

## 4. 위의 설정후에 private repo의 clone이 가능

- gitlab 에 접속할때 위에 설정한 패스워드를 입력

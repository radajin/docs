# WAI-ARIA EXAMPLE

## 1. TAB UI

### 1.1 논리적인 순서에 맞게 엘리먼트를 작성

- 잘못된 예

    ```html
    <div class="tab-interface">
        <ul class="tab-list">
            <li><a href="/">공지사항</a></li>
            <li><a href="/">자료실</a></li>
        </ul>
        <div class="tab-contents">
            <ul class="notice-list">
                <li><a href="/">HTML 모든 것을 알려주마 샘플 활용법</li>
                <li><a href="/">W3C 사이트 리뉴얼 소식 및 공지사항</li>
            </ul>
            <ul class="pds-list">
                <li><ahref="/">시각장애인에 닫힌 모바일 앱</li>
                <li><ahref="/">개도국 정보화 격차 해소</li>
            </ul>
            <a href="/" class="more">더보기</a>
        </div>
    </div>
    ```

- 잘된 예

    ```html
    <div class="tab-interface">
        <div class="tab-contents">
            <div id="notice" class="on">
                <h1><a href="/">공지사항</a></h1>
                <ul class="notice-list">
                    <li><a href="/">HTML 모든 것을 알려주마 샘플 활용법</li>
                    <li><a href="/">W3C 사이트 리뉴얼 소식 및 공지사항</li> 분들이 생각하는 웹 접근성</li>
                </ul>
            <a href="/" class="more" title="공지사항">더보기</a> </div>
            <div id="pds">
                <h1><a href="/">자료실</a></h1>
                <ul class="pds-list">
                    <li><a href="/">시각장애인에 닫힌 모바일 앱</li>
                    <li><a href="/">개도국 정보화 격차 해소</li>
                </ul>
                <a href="/" class="more" title="자료실">더보기</a>
            </div>
        </div>
    </div>
    ```

### 1.2 키보드 인터렉션
- 마우스 입력으로 탭을 동작시킬수 없기 때문에 키보드 입력으로 아래와 같이 동작되도록 해야 함

    | 키보드 | 인터렉션 |
    |-|-|
    | 왼쪽/위쪽 방향키 | 이전 탭으로 이동, 처음 탭일 경우는 마지막 탭으로 이동 |
    | 오른쪽/아래쪽 방향키 | 다음 탭으로 이동, 마지막 탭일 경우는 처음 탭으로 이동 |
    | 탭키 | 활성화 된 순서대로 이동 (비활성화 탭으로 이동하지 않는다) |
    | Home키 | 처음 탭으로 이동 |
    | End키 | 마지막 탭으로 이동 |


## 2. Form UI
- 입력되는 데이터의 제목과 입력지침에 대한 정보가 있어야 한다.

    ```html
    <!-- aria-label="제목" -->
    <!-- aria-describedby="입력지침" -->
    <input type="text" aria-label="아이디" aria-describedby="5~20자의 영문자 소문자 숫자만 사용가능">
    ```

## 3. Button UI
- 버튼은 키보드 접근이 되지 않는 `div` 요소가 `span` 요소로 만들면 안된다.
- 버튼은 `role="button"` 속성을 추가
- 토글 버튼은 버튼 상태에 따라서 `aria-pressed="true"`와 `aria-pressed="false"` 속성을 추가
- `tabindex="0"` 속성을 추가하여 탭키로 이동이 가능하게 함

    |키보드 | 인터랙션 |
    |-|-|
    |엔터키 버튼의 선택 | 토글버튼의 선택 해제 |
    | 스페이스바 버튼의 선택 | 토글버튼의 선택 해제 |

## 4. Layer Popup UI

- 레이어 팝업의 코드는 팝업창 열기 버튼 다음에 위치하는게 좋음
- `layer_area`에 `role="dialog"` 속성을 추가

    ```html
    <div>
        <a href="#" id="layer_open">팝업창열기</a>
    </div>
    <div class="layer_area" id="layer" role="dialog">
        <h1 id="dialogTitle" tabindex="0">사용법</h1>
        <ol>
            <li>팝업창에 포커스이동</li>
            <li>탭키 사용시 순환적 이동</li>
            <li>ESC키/닫기버튼 사용시 팝업종료</li>
            <li>팝업종료시 포커스 이동</li>
        </ol>
        <div>
            <input type="button" value="닫기" id="layer_close"> </div>
    </div>
    ```

    |키보드|인터렉트|
    |-|-|
    |탭키 | 다음 링크로 이동. 마지막 링크에서 탭키 사용 시 처음 링크로 이동 |
    |Esc키 | 레이어 팝업(Layer Popup)을 숨기고 레이어 팝업(Layer Popup)을 호출하기 전 포커스로 이동 |
    |Shift+탭키|이전 링크로 이동. 첫번째 링크에서 Shift + 탭키 사용 시 레이어 팝업(Layer Popup)을 숨기고 레이어 팝업(Layer Popup)을 호출하기 전 포커스로 이동|

## 5. Tooltip UI
- html 요소에 `title` 속성을 추가
- css의 `:hover`와 `:forcus`를 사용하여 포커스 상태에서 툴팁이 보이도록 함

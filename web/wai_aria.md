# WAI-ARIA

## 0. 웹 접근성
- 장애를 가진 사람과 장애를 가지지 않은 사람 모두 웹사이트를 이용할수 있게 하는 방식
- 기본적으로 웹 페이지를 마우스 없이 화면이 보이지 않는 상태에서 이용할수 있도록 해야 함


## 1. WAI-ARIA ?
- Web Accessibility Initiative - Web Accessible Rich Internet Applications
- W3C에서 웹콘텐츠 및 웹 애플리케이션의 접근성과 상호 운용성을 개선하기 위해 기술 명세를 발표


## 2. ARIA Role, Property, State
- 마크업에 역할(Role), 속성(Property), 상태(State) 정보를 추가


### 2.1 역할(Role)
- UI에 포함된 컴포넌트의 기능을 정의
- 컴포넌트가 검색, 네비게이션, 제목등의 기능을 부여
- `a` 컴포넌트에 `button` 역할을 부여
    - 스크린 리더가 링크가 아니라 버튼으로 인지

    ```html
    <a href="/" onclick="playApp()" role="button>재생</a>"
    ```
- Role의 4가지 카테고리
    - Document Structure Role : 문서 구조
    - Abstract Role : 추상 역할
    - Landmark Rolw : 랜드마크 역할
    - Widget Role : 위젯 역할

### 2.2 속성(Property)
- 컴포넌트의 특징이나 상황을 정의
    - 예를 들어 `input`의 입력창이 읽기전용인지, 필수항목인지, 자동완성기능이 있는지, 드래그가 가능한지, 팝업이 뜨는지등을 사용자가 인지할수 있도록 함
- 속성명으로 `aria-`의 접두사 사용
- 속성의 사용 예
    - 아이디 입력
    - `aira-required="true"`를 입력하면 필수 항목임을 알수 있도록 함

    ```html
    <div class="id-area">
        <label>아이디</label>
        <input type="email" id="user-email" aira-required="true">
    </div>
    ```

### 2.3 상태(State)
- 컴포넌트의 상태 정보를 정의
    - expanded : 메뉴가 펼쳐진 상태
    - invalid : 적절하지 못한 값이 입력
    - hidden : 컴포넌트가 숨겨져있는 상태
- 상태의 사용 예
    - `aria-expanded` 속성을 사용하여 메뉴가 접힌상태라면 `false`값을 펼쳐진 상태라면 `true` 값을 지정

    ```html
    <ul>
        <li id="menu" role="treeitem" aria-expanded="true">
            <a>WAI-ARIA 소개</a>
            <ul id="sub-menu" role="group">
                <li id="menu" role="treeitem" aria-expanded="false">
                    <a>WAI-ARIA란?</a>
                </li>
            </ul>
        </li>
        <li id="menu" role="treeitem" aria-expanded="false">
            <a>WAI-ARIA 목적</a>
        </li>
    </ul>
    ```

- 추가된 정보를 스크린 리더 및 보조기기가 인식하여 상호작용을 하게 함
- 스크린 리더의 종류
    - [Sense Reader](http://www.xvtech.com/xvtech/)
    - [nvaccess](https://www.nvaccess.org/)
    - [window-eyes](http://www.gwmicro.com/Window-Eyes/)
    - [JAWS](https://www.freedomscientific.com/Products/Blindness/JAWS)

## 3. How to Use

### 3.1 ARIA 랜드마크`role`과 HTML5의 `element`

- `role="application"`
    - 동일한 역할의 요소 없음.
    - 주로 `<div>` 요소와 같이 그룹 역할을 하는 요소로 대체할 수 있다.

- `role="banner"`
    - 동일한 역할의 요소 없음.
    - 비슷한 의미로 `<header>` 요소를 사용할 수 있으나 `<header role="banner">`로 사용하였다 면 한 페이지에서 한 개의 `<header>` 요소만 사용하길 권장한다.

- `role="navigation"`
    - `<nav>` 요소.
    - 다른 페이지 또는 페이지 내 특정 영역으로 이동하는 링크 콘텐츠 영역으로 주로 메인 메 뉴 및 서브 메뉴 등에 사용할 수 있다.

- `role="main"`
    - `<main>` 요소.
    - 본문의 주요 콘텐츠 영역으로 한 페이지 내에 1개만 사용이 가능하며, `<article>`, `<aside>`, `<footer>` 요소의 하위 요소로 사용할 수 없다

- `role="complementary"`
    - `<aside>` 요소.
    - 주요 콘텐츠와 연관이 적은 의미있는 콘텐츠 영역으로 종종 사이드바로 표현할 수 있다. `<aside>` 영역에는 현재 날씨, 관련된 기사 또는 주식 정보등의 부가 콘텐츠를 포함 할 수 있다.

- `role="form"`
    - `<form>` 요소.
    - 폼과 관련된 요소의 모임을 표현하는 영역으로 서버에 전송될 수 있는 콘텐츠를 포함 할 수 있다.

- `role="search"`
    - 동일한 역할의 요소 없음.
    - 검색의 역할을 담당하는 서식 영역임을 의미하며 `<div>` 또는 `<form>` 요소를 사용하는 것을 권장한다.

- `role="contentinfo"`
    - 동일한 역할의 요소 없음.
    - 비슷한 의미로 `<footer>` 요소를 사용할 수 있으나 `<footer role="contentinfo">`로 사용하였 다면 한 페이지에서 한 개의 `<footer>` 요소만 사용하길 권장한다.


### 3.2 HTML 요소의 기능 변경 제한

- h1 요소에 버튼을 추가할 경우
    - h1에 `role`을 추가 하지 말고 하위 요소로 버튼을 추가한다.

    ```html
    <!-- X -->
    <h1 role="button"> 버튼 </h1>

    <!-- O -->
    <h1><button> 버튼 </button></h1>

    <!-- O -->
    <h1><span role="button" tabindex="0">버튼</span></h1>
    ```

### 3.3 키보드 사용 보장

- 키보드 포커스를 받지 못하는 요소의 경우 `tabindex`설정으로 키보드 포커스를 받을수 있는 상태로 변경한다. `tabindex` 설정이 0보다 작으면 키보드 포커스를 받을수 없는 상태를 의미 한다.

    ```html
    <!-- 키보드 포커스 X -->
    <span role="button" tabindex="-1">버튼</span>
    <!-- 키보드 포커스 O -->
    <span role="button" tabindex="0">버튼</span>
    ```

### 3.4 숨김 콘텐츠

- 콘텐츠를 숨길때는 `aria-hidden="true"`과 `display:none`를 함께 사용한다.

    ```html
    <!-- X -->
    <button role="presentation"> 버튼 </button>

    <!-- X -->
    <button aria-hidden="true"> 버튼 </button>

    <!-- O -->
    button { display : none }
    <button aria-hidden="true"> 버튼 </button>
    ```

### 3.5 레이블 제공

- 대화형 UI의 경우에는 레이블을 제공해야 한다.
    - label 요소를 사용을 권장
    - `aria-label`, `aria-labelledby` 속성을 사용해도 된다.

    ```html
    <!-- label 사용 -->
    <div class="container">
        <label for="user-name">이름</label>
        <input type="text" id="user-name">
    </div>

    <!-- aria-labelledby 사용 -->
    <div>
        <div id="user-name">이름</div>
        <input type="text" aria-labelledby="user-name">
    </div>

    <!-- aria-label 사용 -->
    <button aria-label="닫기" onclick="myDialog.close()"> X </button>
    ```

### 3.6 유효성 검사

- html 요소에 적절하지 않은 WAI-ARIA를 사용하는 경우 문법오류 메시지가 발생한다.
- 예를들어 `h1` 요소에 `role=button`을 사용하는 경우 에러가 발생한다.


## 4. Landmark Role

### 4.1 랜드마크란?

- 웹페이지에서 제공되는 콘텐츠의 유형이 어떤 역할을 하는지 식별할 수 있도록 도와준다.
- 웹페이지의 레이아웃에 랜드마크를 지정하여 해당 레이아웃이 어떤 역할을 하는지 알수 있도록 한다.

    | 구분 | NVDA | JAWS10+ | 센스리더 4+ |
    |-----|------|---------|-----------|
    |다음(Next Landmark)| D | ; | J |
    |이전(Previous Landmark)| Shift + D | Shift + ; | Shift + J |

### 4.2 랜드마크 사용 방법

- role 속성을 통해 레이아웃의 역할을 지정하면 된다.

    ```html
    <div class="container">
        <div role="banner">banner</div>
        <div role="navigation">navigation</div>
        <div role="main">
            <div role="application">application</div>
        </div>
        <div class="complementary">
            <div role="search">search</div>
            <div role="form">form</div>
        </div>
        <div role="contentinfo">contentinfo</div>
    </div>
    ```

### 4.3 랜드마크의 종류

- application
    - 영역을 나타내는 요소로 `div` 요소와 동일

- banner
    - 사이트 로고나 제목

- navigation
    - 네비게이션 영역으로 html5의 `nav`요소와 동일

- main
    - 메인 컨텐츠의 영역을 의미
    - 웹페이지 내에서 한번만 선언할수 있음
    - html5의 `main` 요소와 중복해서 사용하면 안됌

- complementary
    - 주로 메인 컨텐츠에서 분리되어 부가 정보를 나타내는 컨텐츠 영역
    - 날씨 정보나 시세 정보등을 포함
    - html5의 `aside` 요소와 동일

- form
    - 사용자 입력이 가능한 `form` 영역을 의미

- search
    - 검색을 위한 `form` 영역

- contentinfo
    - 메타 데이터를 담을수 있는 영역
    - 저작권, 주소, 연락처등의 `footer` 요소와 비슷한 역할을 함


## 5. Live Region

- 웹페이지가 동적으로 변경되는 경우 변경된 내용과 진행상태를 알리기 위해 사용되는 속성
- `aria-live`, `aria-atomic` 속성을 사용

### 5.1 aria-live
- 업데이트된 요소를 사용자에게 알려주는 방법을 설정
- 기본속성이 `off`
- `polite`로 지정하는 경우 사용자 입력이 끝나면 업데이트된 내용을 사용자에게 알림
- `assertive`로 지정하는 경우 바로 업데이트 된 내용을 사용자에게 알림

### 5.2 aria-atomic
- 사용자에게 업데이트된 부분만 알려줄것인 모두 알려줄것인지를 설정
- 기본속성이 `false` 전체 내용을 모두 알림
- `true` 업데이트된 내용만 알림

    ```html
    <div class="container" aria-live="polite" aria-atomic="true">
        ......
    </div>
    ```

### 5.3 aria-busy

- 업데이트 진행 여부를 설정
- 기본속성이 `false` 업데이트된 내용을 안내
- `ture` 업데이트된 내용을 안내하지 않음

    ```html
    <div class="container" aria-live="polite" aria-busy="true">
        ......
    </div>
    ```

### 5.4 aria-relevant

- 요소 및 텍스트의 추가, 삭제등의 업데이트 정보를 설정
- additions text(기본값) : 요소가 추가되거나 변경되었을때 안내
- additions : 요소가 추가 되었을때 안내
- removals : 요소가 삭제 되었을때 안내
- text : 컨텐츠가 변경되었을때 안내
- all : 요소의 추가 삭제 및 콘텐츠가 변경되었을때 안내

    ```html
    <div class="container" aria-live="polite" aria-relevant="all">
        ......
    </div>
    ```

[wai-aria examples](web/wai_aria_exam.md)

# docs


## python
- basic
- condition
- loop
- function
- class
- decorator


## flask
- basic
- template
- login
    - dance
- connect database
- wsgi
    - gunicorn
    - uwsgi


# database
- mysql
- mongodb
- postgress
- sqlite3


# nginx
- [basic nginx](nginx/index.md)

## gitlab
- [keygen](gitlab/keygen.md)


## linux
- vim
- shell script
- sed
- awk
- nginx


## bigdata
- hadoop
- spark


## web
- [WAI-ARIA : 웹접근성](web/wai_aria.md)


## javascript
- [drag and drop](javascript/tips/drag_and_drop.html)
- [highcharts](javascript/tips/highcharts.html)


## bootstrap


## material-web


## react


## docker


## go lang

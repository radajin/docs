# Nginx

## How to install nginx on ubuntu 16.04

### reference
- https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-16-04


#### 1. Install Nginx

```
$ sudo apt-get update
$ sudo apt-get install nginx
```

#### 2. Manage the Nginx Process

```
# start nginx
$ sudo systemctl start nginx
```

```
# stop nginx
$ sudo systemctl stop nginx
```

```
# restart nginx
$ sudo systemctl restart nginx
```

```
# reload nginx
$ sudo systemctl reload nginx
```


Nginx를 실행한후에 브라우져에 도메인이나 아이디를 입력하면 아래와 같은 메시지를 확인할수 있습니다. 아래의 메시지는 `/var/www/html`에 있습니다
```
Welcome to nginx!
If you see this page, the nginx web server is successfully installed and working. Further configuration is required.

For online documentation and support please refer to nginx.org.
Commercial support is available at nginx.com.

Thank you for using nginx.
```


#### 3. Server Configuration

`/etc/nginx` - Nginx의 설정 디렉토리와 파일은 모두 여기에 있습니다.
`/etc/nginx/nginx.conf`: 기본 Nginx의 설정 파일입니다. 글로벌한 설정은 이 파일에서 수정합니다.
`/etc/nginx/sites-available/`: Nginx의 설정 파일이 있는 디렉토리 입니다. 대부분의 설정을 이 디렉토리에서 설정 파일을 생성하여 설정합니다.

#### 3. Setting Nginx

`/etc/nginx/sites-available/default` 파일을 수정하여 아래와 같은 설정이 가능합니다.

##### 3.1 static file serving
```
server {

    location / {
        root /path/to/html ;
    }

    location /images/ {
        root /path/to/image ;
    }

}
```

##### 3.2 proxy server
```
server {
    listen 8080;  #기술 안하면 default 는 80 port

    location / {
        proxy_pass http://localhost:8080;
    }

}
```

여러개의 포트로 연결 시키고 싶으면 아래와 같이 listen port를 여러개 작성하면 됩니다.
```
server {
    listen 8080;  #기술 안하면 default 는 80 port
    listen 80;

    location / {
        proxy_pass http://localhost:8080;
    }

}
```
